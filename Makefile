# Constants & variables
api_jwt_path = api/config/jwt
lexik_jwt_file = api/config/packages/lexik_jwt_authentication.yaml
git_set_gpgsign = git config commit.gpgsign true

# Run the project
build:
	docker-compose up --build

up:
	docker-compose up -d

stop:
	docker-compose stop

# Installation
jwt:
	mkdir -p api/config/jwt
	openssl genpkey -out $(api_jwt_path)/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
	openssl pkey -in $(api_jwt_path)/private.pem -out $(api_jwt_path)/public.pem -pubout
	sed -i '/pass_phrase/s/"[[:alnum:]]*"/"%env(JWT_PASSPHRASE)%"/' $(lexik_jwt_file)
	mkdir -p graphql/config
	cp -R $(api_jwt_path) graphql/config/jwt

sign-commit:
	$(git_set_gpgsign)
	cd api && $(git_set_gpgsign)
	cd graphql && $(git_set_gpgsign)
	cd vue && $(git_set_gpgsign)

install: jwt build stop

# add-host:
# 	echo "127.0.0.1 api.kickstartup.test kickstartup.test" >> /etc/hosts
