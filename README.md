# KickStartup

## Prérequis
- docker & docker-compose
- php >= 7.4
- composer
- npm
- openssl
- (gnu-sed si sous Mac)*

[*] après l'installation faites attention de bien avoir ajouté `/usr/local/opt/gnu-sed/libexec/gnubin:$PATH` à votre $PATH

## Installation
Clonez le projet et récupérez les submodules
~~~ sh
git clone git@gitlab.com:FooTeam/kickstartup/kickstartup-env.git kickstartup --recurse-submodules
~~~

Initialisez le projet
~~~ sh
make install
~~~
> /!\ Mettez le mot de passe que vous avez rentré pour les certificats dans le .env de l'api et dans le .env de graphql

Ajoutez la règle des commits signés pour tous les projets
~~~ sh
make sign-commit
~~~
